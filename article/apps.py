from django.apps import AppConfig

class ArticleConfig(AppConfig):
    default_auto_field = 'django.db.models.AutoField'
    name = 'article'
