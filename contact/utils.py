import requests

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string


def send_contact_email(contact_entry):
    subject = '[Canudas] Contact message'
    from_email = settings.DEFAULT_FROM_EMAIL
    to_email = [settings.ADMIN_EMAIL]
    text_content = render_to_string('contact/emails/contact.txt', {'contact_entry': contact_entry})
    html_content = render_to_string('contact/emails/contact.html', {'contact_entry': contact_entry})
    msg = EmailMultiAlternatives(subject, text_content, from_email, to_email)
    msg.attach_alternative(html_content, "text/html")
    msg.send()

def add_to_mailchimp_list(email):
    url = f"https://{settings.MAILCHIMP_DATA_CENTER}.api.mailchimp.com/3.0/lists/{settings.MAILCHIMP_LIST_ID}/members/"
    data = {
        "email_address": email,
        "status": "subscribed",
    }
    response = requests.post(url, auth=("apikey", settings.MAILCHIMP_API_KEY), json=data)
    return response.status_code == 200

