from django.db import models

from wagtail.admin.panels import FieldPanel, InlinePanel
from wagtail.fields import RichTextField
from wagtail.images.models import Image
from wagtail.models import Page, Orderable
from modelcluster.fields import ParentalKey

class HomePage(Page):
    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        InlinePanel('carousel_images', label="Carousel images"),
        FieldPanel('body'),
    ]

    def get_context(self, request):
        context = super().get_context(request)
        
        context['carousel_images'] = self.carousel_images.all()

        return context

class HomePageCarouselImage(Orderable):
    page = ParentalKey(HomePage, on_delete=models.CASCADE, related_name='carousel_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    panels = [
        FieldPanel('image'),
    ]

